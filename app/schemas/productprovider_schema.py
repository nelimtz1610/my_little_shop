"Schema for orders"
from app.ma import ma 

class ProductProviderSchema(ma.Schema):
    class Meta:
        fields = (
            "product_id",
            "name_product",
            "name_provider"
        )