"Schema for products"
from app.ma import ma 

class ProductSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "name",
            "purchase_price",
            "sale_price",
            "category_id",
            "barcode",
            "provider_id",
            "created_at",
            "created_by",
            "update_at",
            "update_by",
            "deleted_at",
            "deleted_by"
        )