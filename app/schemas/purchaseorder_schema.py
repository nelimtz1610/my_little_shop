"Schema for purchase orders"
from app.ma import ma 

class PurchaseorderSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "order_id",
            "product_id",
            "price",
            "amount",
            "date_delivery",
            "created_at",
            "created_by",
            "update_at",
            "update_by",
            "deleted_at",
            "deleted_by"
        )