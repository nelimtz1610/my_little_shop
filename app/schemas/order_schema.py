"Schema for orders"
from app.ma import ma 

class OrderSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "provider_id",
            "status",
            "created_at",
            "created_by",
            "update_at",
            "update_by",
            "deleted_at",
            "deleted_by"
        )