"Schema for provider"
from app.ma import ma 

class ProviderSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "name",
            "email",
            "created_at",
            "created_by",
            "update_at",
            "update_by",
            "deleted_at",
            "deleted_by"
        )