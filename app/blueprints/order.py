import json
from flask import Blueprint, request
from datetime import datetime
from app.models.order_model import Order
from app.schemas.order_schema import OrderSchema
from app.responses.generic_response import Responses

order_bp = Blueprint('order', __name__, url_prefix="/order")

@order_bp.get("/")
def index():
    """Get all order that are not deleted"""
    params = request.args
    orders_list = Order().get_all(params)
    orders = OrderSchema(many=True).dump(orders_list)
    return Responses.index_response(orders)

@order_bp.get("/<order_id>")
def show(order_id):
    """Get an order by the given id"""
    order = Order().get_one_by({"id": order_id})
    order = OrderSchema(many=False).dump(order)
    return Responses.show_response(order)

@order_bp.post("")
def create():
    """Create a new order"""
    params = json.loads(request.data)
    params2 = dict(params) #add the data to new variable for the validation 
    keys = params2.keys()
    if("provider_id" not in keys or "status" not in keys):
        return Responses.date_invalid("Missing fields")
    if(params['provider_id'] and isinstance(params['provider_id'], int) and (int(params['provider_id'])>0)) and (params['status'] and isinstance(params['status'], str) and len(params['status'])>0):
        created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        params['created_by'] = "natalia"
        params['created_at'] = created_at
        order = Order(**params,)
        order.create()
        new_order = OrderSchema(many=False).dump(order.get_one_by(params))
        return Responses.create_response(new_order)
    return Responses.date_invalid(params)

@order_bp.put("/<order_id>")
def update(order_id):
    """Update the order according to the given id"""
    body = json.loads(request.data)
    params3 = dict(body)
    keys = params3.keys()
    if 'provider_id'in keys:
        if not ((params3['provider_id'] and isinstance (params3['provider_id'], int) and (int(params3['provider_id']) >0))):
            return Responses.date_invalid(body['provider_id'])
    if 'status'in keys:
        if not ((params3['status'] and isinstance (params3['status'], str) and len(params3['status']) >0)):
            return Responses.date_invalid(body['status'])
    
    updated_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    body['updated_at'] = updated_at
    updated_order = Order().update(order_id, body)
    if updated_order:
        order = OrderSchema(many=False).dump(updated_order)
        return Responses.update_response(order)
    return Responses.not_found_response({"id": order_id})

@order_bp.delete("/<order_id>")
def logic_delete(order_id):
    """Make a logical delete of an order"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    order = Order().update(order_id, {"deleted_at": deleted_at})
    if order:
        order = OrderSchema(many=False).dump(order)
        return Responses.logical_delete(order)
    return Responses.not_found_response({"id": order_id})