import json
from flask import Blueprint, request
from datetime import datetime
from app.models.provider_model import Provider
from app.schemas.provider_schema import ProviderSchema
from app.responses.generic_response import Responses

provider_bp = Blueprint('provider', __name__, url_prefix="/provider")

@provider_bp.get("/")
def index():
    """Get all providers that are not deleted"""
    params = request.args
    providers_list = Provider().get_all(params)
    providers = ProviderSchema(many=True).dump(providers_list)
    return Responses.index_response(providers)

@provider_bp.get("/<provider_id>")
def show(provider_id):
    """Get an product by the given id"""
    provider = Provider().get_one_by({"id": provider_id})
    provider = ProviderSchema(many=False).dump(provider)
    return Responses.show_response(provider)

@provider_bp.post("")
def create():
    """Create a new provider"""
    params = json.loads(request.data)
    params = json.loads(request.data) #add the data json to variable
    params2 = dict(params) #add the data to new variable for the validation 
    keys = params2.keys()
    if("name" not in keys or "email" not in keys):
        return Responses.date_invalid("Missing fields")
    if(params['name'] and isinstance(params['name'], str) and len(params['name'])>0) and (params['email'] and isinstance(params['email'], str) and len(params['email'])>0):
        created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        params['created_by'] = "natalia"
        params['created_at'] = created_at
        provider = Provider(**params,)
        provider.create()
        new_provi = ProviderSchema(many=False).dump(provider.get_one_by(params))
        return Responses.create_response(new_provi)
    return Responses.date_invalid(params)

@provider_bp.put("/<provider_id>")
def update(provider_id):
    """Update the product according to the given id"""
    body = json.loads(request.data)
    params3 = dict(body)
    keys = params3.keys()
    if 'name'in keys:
        if not ((params3['name'] and isinstance (params3['name'], str) and len(params3['name']) >0)):
            return Responses.date_invalid(body['name'])
    if 'email'in keys:
        if not ((params3['email'] and isinstance (params3['email'], str) and len(params3['email']) >0)):
            return Responses.date_invalid(body['email'])
        
    updated_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    body['updated_at'] = updated_at
    updated_pro = Provider().update(provider_id, body)
    if updated_pro:
        provider = ProviderSchema(many=False).dump(updated_pro)
        return Responses.update_response(provider)
    return Responses.not_found_response({"id": provider_id})

@provider_bp.delete("/<provider_id>")
def logic_delete(provider_id):
    """Make a logical delete of an provider"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    provider = Provider().update(provider_id, {"deleted_at": deleted_at})
    if provider:
        provider = ProviderSchema(many=False).dump(provider)
        return Responses.logical_delete(provider)
    return Responses.not_found_response({"id": provider_id})