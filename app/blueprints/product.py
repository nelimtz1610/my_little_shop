import json
from flask import Blueprint, request
from datetime import datetime
from app.models.product_model import Product
from app.schemas.product_schema import ProductSchema
from app.schemas.productprovider_schema import ProductProviderSchema
from app.responses.generic_response import Responses

#path
product_bp = Blueprint('product', __name__, url_prefix="/product")

#for get add /: back dic type python with of data of the json
@product_bp.get("/")
def index():
    """Get all products that are not deleted"""
    params = request.args
    products_list = Product().get_all(params)
    products = ProductSchema(many=True).dump(products_list)
    return Responses.index_response(products)

#get id 
@product_bp.get("/<product_id>")
def show(product_id):
    """Get an product by the given id"""
    product = Product().get_one_by({"id": product_id})
    product = ProductSchema(many=False).dump(product)
    return Responses.show_response(product)

#Post : back to dictionary type python with the data json
@product_bp.post("")
def create():
    """Create a new product"""
    params = json.loads(request.data) #add the data json to variable
    params2 = dict(params) #add the data to new variable for the validation 
    keys = params2.keys()
    if("name" not in keys or "purchase_price" not in keys or "sale_price" not in keys or "category_id" not in keys or "barcode" not in keys or "provider_id" not in keys):
        return Responses.date_invalid("Missing fields")
    if(params['name'] and isinstance(params['name'], str) and len(params['name'])>0) and (params['purchase_price'] and isinstance(params['purchase_price'], float) and (float(params['purchase_price'])>0)) and (params['sale_price'] and isinstance(params['sale_price'], float) and (float(params['sale_price'])>0)) and (params['category_id'] and isinstance(params['category_id'], int) and (int(params['category_id'])>0)) and (params['provider_id'] and isinstance(params['provider_id'], int) and (int(params['provider_id'])>0)) and (params['barcode'] and isinstance(params['barcode'], str) and len(params['barcode'])<14) :
        created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        params['created_by'] = "natalia"
        params['created_at'] = created_at
        product = Product(**params,)
        product.create()
        new_pro = ProductSchema(many=False).dump(product.get_one_by(params))
        return Responses.create_response(new_pro)
    return Responses.date_invalid(params)

#PUT 
@product_bp.put("/<product_id>")
def update(product_id):
    """Update the product according to the given id"""
    body = json.loads(request.data)
    params3 = dict(body)
    keys = params3.keys()
    #validation the data
    if 'name'in keys:
        if not ((params3['name'] and isinstance (params3['name'], str) and len(params3['name']) >0)):
            return Responses.date_invalid(body['name'])
    if 'purchase_price'in keys:
        if not ((params3['purchase_price'] and isinstance (params3['purchase_price'], float) and (float(params3['purchase_price']) >0))):
            return Responses.date_invalid(body['purchase_price'])
    if 'sale_price'in keys:
        if not ((params3['sale_price'] and isinstance (params3['sale_price'], float) and (float(params3['sale_price']) >0))):
            return Responses.date_invalid(body['sale_price'])
    if 'category_id'in keys:
        if not ((params3['category_id'] and isinstance (params3['category_id'], int) and (int(params3['category_id']) >0))):
            return Responses.date_invalid(body['category_id'])
    if 'provider_id'in keys:
        if not ((params3['provider_id'] and isinstance (params3['provider_id'], int) and (int(params3['provider_id']) >0))):
            return Responses.date_invalid(body['provider_id'])
    if 'barcode'in keys:
        if not ((params3['barcode'] and isinstance (params3['barcode'], str) and len(params3['barcode']) >0)<14):
            return Responses.date_invalid(body['barcode'])
   #update data to database
    updated_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    body['updated_at'] = updated_at
    updated_pro = Product().update(product_id, body)
    if updated_pro:
        product = ProductSchema(many=False).dump(updated_pro)
        return Responses.update_response(product)
    return Responses.not_found_response({"id": product_id})

@product_bp.delete("/<product_id>")
def logic_delete(product_id):
    """Make a logical delete of an product"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    product = Product().update(product_id, {"deleted_at": deleted_at})
    if product:
        product = ProductSchema(many=False).dump(product)
        return Responses.logical_delete(product)
    return Responses.not_found_response({"id": product_id})


@product_bp.get("productprovider/<provider_id>")
def getproduct(provider_id):
    productprovider_list = Product().get_product_by_provider(provider_id)
    productprovider = ProductProviderSchema(many=True).dump(productprovider_list)
    return Responses.index_response(productprovider)


