import json
from flask import Blueprint, request
from datetime import datetime
from app.models.purchaseorder_model import Purchaseorder
from app.schemas.purchaseorder_schema import PurchaseorderSchema
from app.responses.generic_response import Responses

purchaseorder_bp = Blueprint('purchaseorder', __name__, url_prefix="/purchaseorder")

@purchaseorder_bp.get("/")
def index():
    """Get all order that are not deleted"""
    params = request.args
    purchaseorders_list = Purchaseorder().get_all(params)
    purchaseorders = PurchaseorderSchema(many=True).dump(purchaseorders_list)
    return Responses.index_response(purchaseorders)

@purchaseorder_bp.get("/<purchaseorder_id>")
def show(purchaseorder_id):
    """Get an order by the given id"""
    purchaseorder = Purchaseorder().get_one_by({"id": purchaseorder_id})
    purchaseorder = PurchaseorderSchema(many=False).dump(purchaseorder)
    return Responses.show_response(purchaseorder)

@purchaseorder_bp.post("")
def create():
    """Create a new order"""
    params = json.loads(request.data)
    params2 = dict(params) #add the data to new variable for the validation 
    keys = params2.keys()
    if("order_id" not in keys or "product_id" not in keys or "price" not in keys or "amount" not in keys or "date_delivery" not in keys):
        return Responses.date_invalid("Missing fields")
    if(params['order_id'] and isinstance(params['order_id'], int) and (int(params['order_id'])>0)) and (params['product_id'] and isinstance(params['product_id'], int) and (int(params['product_id'])>0)) and (params['price'] and isinstance(params['price'], float) and (float(params['price'])>0)) and (params['amount'] and isinstance(params['amount'], int) and (int(params['amount'])>0)): 
        try:
            date_delivery = datetime.strptime(params['date_delivery'], "%Y-%m-%d")
        except ValueError:
            return Responses.date_invalid(params['date_delivery'])
    created_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    params['created_by'] = "natalia"
    params['created_at'] = created_at
    purchaseorder = Purchaseorder(**params,)
    purchaseorder.create()
    new_purchaseorder = PurchaseorderSchema(many=False).dump(purchaseorder.get_one_by(params))
    return Responses.create_response(new_purchaseorder)

@purchaseorder_bp.put("/<purchaseorder_id>")
def update(purchaseorder_id):
    """Update the purchaseorder according to the given id"""
    body = json.loads(request.data)
    params3 = dict(body)
    keys = params3.keys()
    if 'order_id'in keys:
        if not ((params3['order_id'] and isinstance (params3['order_id'], int) and (int(params3['order_id']) >0))):
            return Responses.date_invalid(body['order_id'])
    if 'product_id'in keys:
        if not ((params3['product_id'] and isinstance (params3['product_id'], int) and (int(params3['product_id']) >0))):
            return Responses.date_invalid(body['product_id'])
    if 'price'in keys:
        if not ((params3['price'] and isinstance (params3['price'], float) and (float(params3['price']) >0))):
            return Responses.date_invalid(body['price'])
    if 'amount'in keys:
        if not ((params3['amount'] and isinstance (params3['amount'], int) and (int(params3['amount']) >0))):
            return Responses.date_invalid(body['amount'])
        try:
            date_delivery = datetime.strptime(body['date_delivery'], "%Y-%m-%d")
        except ValueError:
            return Responses.date_invalid(body['date_delivery'])
        
    updated_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    body['updated_at'] = updated_at
    updated_purchaseorder = Purchaseorder().update(purchaseorder_id, body)
    if updated_purchaseorder:
        purchaseorder = PurchaseorderSchema(many=False).dump(updated_purchaseorder)
        return Responses.update_response(purchaseorder)
    return Responses.not_found_response({"id": purchaseorder_id})

@purchaseorder_bp.delete("/<purchaseorder_id>")
def logic_delete(purchaseorder_id):
    """Make a logical delete of an order"""
    deleted_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    purchaseorder = Purchaseorder().update(purchaseorder_id, {"deleted_at": deleted_at})
    if purchaseorder:
        purchaseorder = PurchaseorderSchema(many=False).dump(purchaseorder)
        return Responses.logical_delete(purchaseorder)
    return Responses.not_found_response({"id": purchaseorder_id})