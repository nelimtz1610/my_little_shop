"Model for purchase order"
from app.db import db

class Purchaseorder(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    order_id = db.Column(db.Integer, nullable= False)
    product_id = db.Column(db.Integer, nullable= False)
    price = db.Column(db.Float, nullable= False)
    amount = db.Column(db.Integer, nullable= False)
    date_delivery = db.Column(db.Date, nullable= False)
    created_at = db.Column(db.Date, nullable= False)
    created_by = db.Column(db.String(50), nullable= False)
    updated_at = db.Column(db.Date, nullable= True)
    updated_by = db.Column(db.String(50), nullable= True)
    deleted_at = db.Column(db.Date, nullable= True)
    deleted_by = db.Column(db.String(50), nullable= True)
    
    def get_all(self, params=None):
        """Get all order that are not deleted"""
        return self.query.filter_by(deleted_at=None, **params).all()

    def get_one_by(self, params):
        """Get the first resource by the given params"""
        return self.query.filter_by(**params).first()

    def create(self):
        """Create a new order in DB"""
        db.session.add(self)
        db.session.commit()

    def update(self, purchaseorder_id, params):
        """Update the order data in DB"""
        purchaseorder = self.get_one_by({"id": purchaseorder_id, "deleted_at": None})
        if purchaseorder:
            for param, value in params.items():
                setattr(purchaseorder, param, value)
            db.session.commit()
            return self.get_one_by({"id": purchaseorder_id})
        return None
