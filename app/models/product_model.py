"Model for product"
from app.db import db

class Product(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    name = db.Column(db.String(50), nullable= False)
    purchase_price = db.Column(db.Float, nullable= False)
    sale_price = db.Column(db.Float, nullable= False)
    category_id = db.Column(db.Integer, nullable= False)
    barcode = db.Column(db.String(13), nullable= False)
    provider_id = db.Column(db.Integer, nullable= False)
    created_at = db.Column(db.Date, nullable= False)
    created_by = db.Column(db.String(50), nullable= False)
    updated_at = db.Column(db.Date, nullable= True)
    updated_by = db.Column(db.String(50), nullable= True)
    deleted_at = db.Column(db.Date, nullable= True)
    deleted_by = db.Column(db.String(50), nullable= True)
    
    def get_all(self, params=None):
        """Get all product that are not deleted"""
        return self.query.filter_by(deleted_at=None, **params).all()

    def get_one_by(self, params):
        """Get the first resource by the given params"""
        return self.query.filter_by(**params).first()

    def create(self):
        """Create a new prouct in DB"""
        db.session.add(self)
        db.session.commit()

    def update(self, pro_id, params):
        """Update the product data in DB"""
        product = self.get_one_by({"id": pro_id, "deleted_at": None})
        if product:
            for param, value in params.items():
                setattr(product, param, value)
            db.session.commit()
            return self.get_one_by({"id": pro_id})
        return None
    
    def get_product_by_provider(self, provider_id):
        query = f"""SELECT product.id as product_id ,product.name as name_product, provider.name as name_provider FROM my_little_shop.product inner join my_little_shop.provider ON product.provider_id = provider.id where provider_id='{provider_id}'
        """
        with db.engine.connect() as con:
            result = con.exec_driver_sql(query)
            return result
        