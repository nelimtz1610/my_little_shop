"Model for provider"
from app.db import db

class Provider(db.Model):
    id = db.Column(db.Integer, primary_key= True)
    name = db.Column(db.String(50), nullable= False)
    email = db.Column(db.String(50), nullable= False)
    created_at = db.Column(db.Date, nullable= False)
    created_by = db.Column(db.String(50), nullable= False)
    updated_at = db.Column(db.Date, nullable= True)
    updated_by = db.Column(db.String(50), nullable= True)
    deleted_at = db.Column(db.Date, nullable= True)
    deleted_by = db.Column(db.String(50), nullable= True)
    
    def get_all(self, params=None):
        """Get all product that are not deleted"""
        return self.query.filter_by(deleted_at=None, **params).all()

    def get_one_by(self, params):
        """Get the first resource by the given params"""
        return self.query.filter_by(**params).first()

    def create(self):
        """Create a new provider in DB"""
        db.session.add(self)
        db.session.commit()

    def update(self, provi_id, params):
        """Update the provider data in DB"""
        provider = self.get_one_by({"id": provi_id, "deleted_at": None})
        if provider:
            for param, value in params.items():
                setattr(provider, param, value)
            db.session.commit()
            return self.get_one_by({"id": provi_id})
        return None
