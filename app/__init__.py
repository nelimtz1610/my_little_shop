"""Init App file"""
from flask import Flask
from app.blueprints.product import product_bp
from app.blueprints.provider import provider_bp
from app.blueprints.order import order_bp
from app.blueprints.purchaseorder import purchaseorder_bp
from app.db import db
from app.config import Config

app = Flask(__name__)
app.config.from_object(Config())

@app.get('/')
def test():
    return 'Hello'

app.register_blueprint(product_bp)
app.register_blueprint(provider_bp)
app.register_blueprint(order_bp)
app.register_blueprint(purchaseorder_bp)

db.init_app(app)