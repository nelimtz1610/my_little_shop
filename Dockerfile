FROM python:latest
COPY . /my_little_shop
WORKDIR /my_little_shop
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["manage.py"]