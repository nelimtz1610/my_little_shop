CREATE DATABASE  IF NOT EXISTS `my_little_shop` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `my_little_shop`;
-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: my_little_shop
-- ------------------------------------------------------
-- Server version	5.7.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'lacteos','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(2,'abarrotes','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(3,'frutas y verduras','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(4,'bebidas','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(5,'higiene personal','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(6,'panaderia','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `provider_id` int(3) NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_provider_id_` (`provider_id`),
  CONSTRAINT `fk_provider_id_` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,1,'entregado','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(2,2,'pedido','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(3,3,'pedido','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(4,4,'entregado','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(5,5,'entregado','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(6,2,'entregado','2022-07-06 04:00:40','natalia','2022-07-06 04:02:42','natalia','2022-07-06 04:03:25',NULL);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_product` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `order_id` int(3) NOT NULL,
  `product_id` int(3) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `amount` int(5) NOT NULL,
  `date_delivery` date NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_unique` (`product_id`,`order_id`),
  KEY `fk_purchase_order_id` (`order_id`),
  CONSTRAINT `fk_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `fk_purchase_order_id` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_product`
--

LOCK TABLES `order_product` WRITE;
/*!40000 ALTER TABLE `order_product` DISABLE KEYS */;
INSERT INTO `order_product` VALUES (1,1,1,50.00,10,'2022-06-25','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(2,2,2,30.00,8,'2022-06-25','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(3,3,4,90.00,5,'2022-06-25','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(4,4,3,200.00,11,'2022-06-25','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(5,5,5,140.00,9,'2022-06-25','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `order_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `purchase_price` decimal(6,2) NOT NULL,
  `sale_price` decimal(6,2) NOT NULL,
  `category_id` int(3) NOT NULL,
  `barcode` varchar(13) NOT NULL,
  `provider_id` int(3) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_category` (`category_id`),
  KEY `fk_id_provider` (`provider_id`),
  CONSTRAINT `fk_id_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `fk_id_provider` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'leche',20.00,30.00,1,'5896321478965',1,'2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(2,'jitomate',15.00,20.00,3,'5134672091365',2,'2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(3,'refresco manzana',27.00,35.00,4,'9840631534498',4,'2022-07-05 00:00:00','natalia',NULL,NULL,'2022-07-05 18:45:49','natalia'),(4,'shampoo',40.00,45.00,5,'7410243697025',3,'2022-07-05 00:00:00','natalia','2022-07-05 19:06:05','natalia',NULL,NULL),(5,'arroz',17.00,23.00,2,'4710852900324',5,'2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(6,'papaya',20.00,30.00,3,'0980907123521',2,'2022-07-05 18:38:09','natalia',NULL,NULL,NULL,NULL),(7,'yogurt',15.00,20.00,1,'0980907123521',1,'2022-07-05 19:16:09','natalia',NULL,NULL,NULL,NULL),(8,'pasta',15.00,19.50,5,'6789845367890',3,'2022-07-05 21:09:28','natalia','2022-07-05 21:13:52',NULL,NULL,NULL),(9,'pasta',20.00,30.00,5,'6789845367890',3,'2022-07-05 21:10:26','natalia',NULL,NULL,'2022-07-05 21:12:06',NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider`
--

DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `provider` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider`
--

LOCK TABLES `provider` WRITE;
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
INSERT INTO `provider` VALUES (1,'lala','lala@gmail.com','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(2,'verduras guadalajara','verdurita@gmail.com','2022-07-05 00:00:00','natalia','2022-07-05 21:23:33','natalia',NULL,NULL),(3,'pantene','pantene@gmail.com','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(4,'cocacola','cocacola@gmail.com','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(5,'semillas la costa','semillita@gmail.com','2022-07-05 00:00:00','natalia',NULL,NULL,NULL,NULL),(6,'sabritas','sabritas@gmail.com','2022-07-05 21:20:26','natalia',NULL,NULL,NULL,NULL),(7,'jumex','jumex@gmail.com','2022-07-05 21:27:52','natalia',NULL,NULL,'2022-07-05 21:28:24',NULL);
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-05 23:14:31
