use my_little_shop;

/*Productos*/
DROP TABLE IF EXISTS product ;
CREATE TABLE product(
id int(3) PRIMARY KEY NOT NULL AUTO_INCREMENT,
name VARCHAR(50) NOT NULL,
purchase_price DECIMAL(6,2) NOT NULL,
sale_price DECIMAL(6,2) NOT NULL,
category_id int(3)NOT NULL,
barcode VARCHAR(13)NOT NULL,
provider_id INT(3)NOT NULL,
created_at DATETIME NOT NULL,
created_by VARCHAR(50) NOT NULL,
updated_at DATETIME NULL,
updated_by VARCHAR(50) NULL,
deleted_at DATETIME NULL,
deleted_by VARCHAR(50) NULL
);
/*Add data for products */
INSERT INTO product(id,name,purchase_price,sale_price,category_id,barcode, provider_id, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by)
VALUES  (1, 'leche', 29.50, 35, 1, 5896321478965, 1, CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
		(2, 'jitomate', 15, 20, 3, 5134672091365, 2, CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
        (3, 'refresco manzana', 27, 35, 4, 9840631534498, 4,CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
        (4, 'shampoo', 36, 45, 5, 7410243697025, 3,CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
		(5, 'arroz', 17, 23, 2, 4710852900324, 5,CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL);


/*Category*/
DROP TABLE IF EXISTS category;
CREATE TABLE category(
id int(3) PRIMARY KEY NOT NULL AUTO_INCREMENT,
name VARCHAR(50) NOT NULL,
created_at DATETIME NOT NULL,
created_by VARCHAR(50) NOT NULL,
updated_at DATETIME NULL,
updated_by VARCHAR(50) NULL,
deleted_at DATETIME NULL,
deleted_by VARCHAR(50) NULL
);
/*Add data for category */
INSERT INTO category(id, name, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by)
VALUES (1, 'lacteos', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(2, 'abarrotes', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(3, 'frutas y verduras', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
 (4, 'bebidas', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
 (5, 'higiene personal', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
 (6, 'panaderia', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL);


/*Provider*/
DROP TABLE IF EXISTS provider;
CREATE TABLE provider(
id int(3) PRIMARY KEY NOT NULL AUTO_INCREMENT,
name VARCHAR(50)NOT NULL ,
email VARCHAR(50)NOT NULL,
created_at DATETIME NOT NULL,
created_by VARCHAR(50) NOT NULL,
updated_at DATETIME NULL,
updated_by VARCHAR(50) NULL,
deleted_at DATETIME NULL,
deleted_by VARCHAR(50) NULL
);
/*Add data for provider */
INSERT INTO provider (id, name, email, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by) 
VALUES
(1, 'lala', 'lala@gmail.com', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(2, 'verduras guadalajara', 'alpura@gmail.com', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(3, 'pantene', 'pantene@gmail.com', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(4, 'cocacola', 'cocacola@gmail.com', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(5, 'semillas la costa', 'semillita@gmail.com', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL);


/*Purchase order*/
DROP TABLE IF EXISTS ´order´;
CREATE TABLE ´order´(
id int(3) PRIMARY KEY NOT NULL AUTO_INCREMENT,
provider_id int(3)NOT NULL,
status VARCHAR(50)NOT NULL,
created_at DATETIME NOT NULL,
created_by VARCHAR(50) NOT NULL,
updated_at DATETIME NULL,
updated_by VARCHAR(50) NULL,
deleted_at DATETIME NULL,
deleted_by VARCHAR(50) NULL
);
/*Add data for pruchase order */
INSERT INTO ´order´(id, provider_id, status, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by) VALUES
(1, 1, 'entregado', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(2, 2, 'pedido', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(3, 3, 'pedido', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(4, 4, 'entregado', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(5, 5, 'entregado', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL);


/*Purchase order product*/
DROP TABLE IF EXISTS order_product;
CREATE TABLE order_product(
id int(3) PRIMARY KEY NOT NULL AUTO_INCREMENT,
order_id int(3)NOT NULL,
product_id int(3)NOT NULL,
price DECIMAL(6,2)NOT NULL,
amount int(5)NOT NULL,
date_delivery DATE NOT NULL,
created_at DATETIME NOT NULL,
created_by VARCHAR(50) NOT NULL,
updated_at DATETIME NULL,
updated_by VARCHAR(50) NULL,
deleted_at DATETIME NULL,
deleted_by VARCHAR(50) NULL,
UNIQUE INDEX product_unique (product_id, order_id),
CONSTRAINT price_greather_zero CHECK ( price > 0)
);
/*Add data for purchase order product */
INSERT INTO order_product (id, order_id, product_id, price, amount, date_delivery, created_at, created_by, updated_at, updated_by, deleted_at, deleted_by) VALUES
(1, 1, 1, 50, 10, '2022-06-25', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(2, 2, 2, 30, 8, '2022-06-25', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(3, 3, 4, 90, 5, '2022-06-25', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(4, 4, 3, 200, 11, '2022-06-25', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL),
(5, 5, 5, 140, 9,'2022-06-25', CURRENT_DATE(), 'natalia', NULL,NULL,NULL,NULL);




/*foreign key between category and products*/
ALTER TABLE product
ADD CONSTRAINT fk_id_category
FOREIGN KEY(category_id)
REFERENCES category(id);

/*foreign key between provider and products*/
ALTER TABLE product
ADD CONSTRAINT fk_id_provider
FOREIGN KEY(provider_id)
REFERENCES provider(id);

/*foreign key between provider and purchase_order*/
ALTER TABLE purchase_order
ADD CONSTRAINT fk_provider_id_
FOREIGN KEY(provider_id)
REFERENCES provider(id);

/*foreign key between order,  purchase_order_prodcut*/
ALTER TABLE order_product
ADD CONSTRAINT fk_order_id
FOREIGN KEY(order_id)
REFERENCES ´order´(id);

/*foreign key between order, product, purchase_order_prodcut*/
ALTER TABLE order_product
ADD CONSTRAINT fk_product_id
FOREIGN KEY(product_id)
REFERENCES product(id);
